#include <stdio.h>
#include "immintrin.h"
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
// gcc -mavx512f -mavx512bw -O2 -o 1_test_add  1_test_add.c -Wall -Wextra -std=gnu++11
//  ./1_test_add
__attribute__((aligned(64))) int64_t a[100000] = {0};
__attribute__((aligned(64))) int64_t b[100000] = {0};
__attribute__((aligned(64))) int64_t ab[100000] = {0};

__attribute__((aligned(64))) int64_t avx512[100000] = {0};
int main()
{

    int number = 800;
    struct timeval start, end, diff;
    int32_t f[16] = {0};

    float val[16];
    __m512 simd1, simd2, simd3, simd4;
    __m512i simd5, simd6, simd7;

    // __mmask16 m16z = 0;
    // __mmask16 m16s = 0xAAAA;
    // __mmask16 m16a = 0xFFFF;

    int i = 0;
    int j = 0;
    int k = 0;
    srand(time(NULL)); //打亂每次出現一樣的亂數
    for (i = 0; i < number; i++)
    {
        // rand() % (最大值-最小值+1)+最小值
        a[i] = rand() % 9 + 1;
        // printf("%d ,", a[i]);
    }
    printf("\n");
    for (i = 0; i < number; i++)
    {
        // rand() % (最大值-最小值+1)+最小值
        b[i] = 20;
        // printf("%d ,", b[i]);
    }
    while (1)
    {
        if ((a[k] == 0))
        {
            break;
        }
        else
        {
            k++;
        }
    }
    printf("\ncount_a = %ld\n", k);
    //----------------------------------ab--------------------------------------
    int k_2 = 0;
    gettimeofday(&start, NULL);
    for (i = 0; i < number; i++)
    {
        ab[i] = a[i] * b[i];
        // printf("%d, ", ab[i]);
    }
    // for (int q = 0; q < 64; q++)
    // {
    //     printf("%d ,", ab[q]);
    // }
    gettimeofday(&end, NULL);
    timersub(&end, &start, &diff);
    printf("\n----------------------------\n");
    printf("Elapsed time Origin: %ld(usec)\n", (long int)diff.tv_usec);
    printf("----------------------------\n");
    while (1)
    {
        if ((ab[k_2] == 0))
        {
            break;
        }
        else
        {
            k_2++;
        }
    }

    printf("ori[%d] = %d\n", number / 8, ab[number / 8]);
    printf("ori[%d] = %d\n", number / 8 * 2, ab[number / 8 * 2]);
    printf("ori[%d] = %d\n", number / 8 * 3, ab[number / 8 * 3]);
    printf("ori[%d] = %d\n", number / 8 * 4, ab[number / 8 * 4]);
    printf("ori[%d] = %d\n", number / 8 * 5, ab[number / 8 * 5]);
    printf("ori[%d] = %d\n", number / 8 * 6, ab[number / 8 * 6]);
    printf("ori[%d] = %d\n", number / 8 * 7, ab[number / 8 * 7]);
    printf("ori[%d] = %d\n", number / 8 * 8, ab[number / 8 * 8]);
    printf("count_ab = %d\n", k_2);

    //-------------------------AVX512---------------------------------
    struct timeval start_avx, end_avx, diff_avx;
    int avx_count = 0;

    gettimeofday(&start_avx, NULL);
    for (int u = 0; u < number / 8; u++)
    {
        simd5 = _mm512_load_epi32(&a[0 + 8 * u]);
        simd6 = _mm512_load_epi32(&b[0 + 8 * u]);
        simd7 = _mm512_mul_epi32(simd5, simd6);
        _mm512_store_epi32(&avx512[0 + 8 * u], simd7);

        // printf("\n");
        // for (int q = 0; q < 16; q++)
        // {
        //     printf("%d ,", avx512[q +8*u]);
        // }
    }
    gettimeofday(&end_avx, NULL);
    timersub(&end_avx, &start_avx, &diff_avx);
    printf("\n----------------------------\n");
    printf("Elapsed time AVX: %ld(usec)\n", (long int)diff_avx.tv_usec);
    printf("----------------------------\n");
    for (int q = 0; q < 64; q++)
    {
        printf("%d ,", avx512[q]);
    }
    printf("\n");
    while (1)
    {
        if (avx512[avx_count] == 0)
        {
            break;
        }
        else
        {
            // printf("%d ,",avx512[avx_count]);
            avx_count++;
        }
    }
    printf("avx[%d] = %d\n", number / 8, avx512[number / 8]);
    printf("avx[%d] = %d\n", number / 8 * 2, avx512[number / 8 * 2]);
    printf("avx[%d] = %d\n", number / 8 * 3, avx512[number / 8 * 3]);
    printf("avx[%d] = %d\n", number / 8 * 4, avx512[number / 8 * 4]);
    printf("avx[%d] = %d\n", number / 8 * 5, avx512[number / 8 * 5]);
    printf("avx[%d] = %d\n", number / 8 * 6, avx512[number / 8 * 6]);
    printf("avx[%d] = %d\n", number / 8 * 7, avx512[number / 8 * 7]);
    printf("avx[%d] = %d\n", number / 8 * 8, avx512[number / 8 * 8]);
    printf("avx_count = %d\n", avx_count);
    return 0;
}